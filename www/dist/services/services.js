var accessTokenObject = {
    get: function() {
        return window.localStorage.access_token;
    },
    set: function(token) {
        return window.localStorage.access_token = token;
    },
    delete: function() {
        delete window.localStorage.access_token;
    }
};

var appServices;
(function() {
    var read_count = 0;
    appServices = angular.module('app.services', [])
      .factory('AccessToken', function($http, SERVER, $window, $rootScope) {
          return accessTokenObject;
      })

      .factory('User', function($http, SERVER, $window, $rootScope) {
          var device_platform, device_token;
          var isLoggedIn = false;
          var o = {
              data: null
          };

          o.getUserData = function() {
              if(!o.data)
              {
                  o.data = JSON.parse($window.localStorage.user);
              }
              return o.data;
          };
          o.setDeviceData = function(platform, token) {
              var oldToken;

              device_platform = platform;
              device_token = token;
              console.log(device_platform, device_token);

              if ($window.localStorage.deviceToken != token) {
                oldToken = $window.localStorage.deviceToken;
                $window.localStorage.deviceToken = token;

                if (isLoggedIn) {
                  var url = SERVER.api_url + "users/device_tokens";
                  return $http({
                      method: 'POST',
                      data: {
                          id: User.id,
                          old_token: oldToken,
                          new_token: token
                      },
                      url : url,
                      headers: {
                          Authorization: "Bearer "+ accessTokenObject.get()
                      }
                  });
                }
              }
          };
          o.signIn = function(email, password) {
              var clientId = SERVER.client_id;
              var apiURL = SERVER.api_url;
              device_token = $window.localStorage.deviceToken;

              if ($rootScope.browserMode) {
                  apiURL = SERVER.api_url = "https://l.recognizeapp.com:50000/api/v2/";
                  clientId = SERVER.client_id = "a2aa1cd437b9b6e0bb6ac07fea569459c95771efbbef9d87d393ac9a46d7a110";
              }


              return $http({
                  method: "POST",
                  url: apiURL + 'auth',
                  timeout: 10000,
                  data: {
                      email: email,
                      password: password,
                      client_id: clientId,
                      device_platform: device_platform,
                      device_token: device_token
                  }
              }).then(function(response){
                  console.log(response.data.auth.access_token);
                  accessTokenObject.set(response.data.auth.access_token);
                  o.data = response.data.auth.user;
                  $window.localStorage.user = JSON.stringify(o.data);
                  console.log("login success");
                  isLoggedIn = true;
              }, function(response){
                  o.data = null;
                  console.log("login fail");
                  console.log(response);
              });
          };
          o.signOut = function() {
              o.data = null;
              accessTokenObject.delete();
              $window.localStorage.removeItem('user');
              $window.localStorage.removeItem('latest_recognitions');
          };
          return o;
      })

      .factory('Recognitions', function($rootScope, $http, SERVER, $window, User) {
          var offlineMode = false;
          var has_more = true;
          var o = {
              recognitions: [],
              selected: null,
              response: null
          };
          var storedRecognitions = $window.localStorage.latest_recognitions;

          o.loadMore = function() {
              var url = SERVER.api_url + "recognitions?page=" + (read_count + 1) + "&per_page=10";

              checkConnection();

              return $http({
                  method: 'GET',
                  url : url,
                  headers: {
                      Authorization: "Bearer "+ accessTokenObject.get()
                  }
              }).then(function(response){
                  var map = {};

                  if(read_count === 0 ) {
                      $window.localStorage.latest_recognitions = JSON.stringify(response.data.recognitions);
                  }

                  o.recognitions = o.recognitions.concat(response.data.recognitions);

                  o.recognitions = o.recognitions.filter(function(recognition) {
                      var shouldAdd = true;

                      if (map[recognition.id]) {
                        shouldAdd = false;
                      } else {
                        map[recognition.id] = recognition;
                      }

                      return shouldAdd;
                  });

                  for(var item in o.recognitions) {

                      if(map[o.recognitions[item].id]) {

                      } else {

                        map[o.recognitions[item].id] = o.recognitions[item];
                      }
                  }

                  read_count++;

                  console.log('read more success');
                  $rootScope.$broadcast('scroll.refreshComplete');
                  
                  if (read_count >= response.data.total_pages) {
                      has_more = false;
                      return 'end';
                  } else {
                    return 'success';
                  }
                  

              }, function(response){
                  console.log('read more fail');
                  return 'fail';
              });
          };

          o.sendRecognition = function(badge, recipients, message) {
              checkConnection();
              return $http({
                  method : "POST",
                  url:SERVER.api_url + "recognitions",
                  data: {
                      badge: badge.id,
                      message: message,
                      recipients: getRecipientStr(recipients)
                  },
                  timeout: 15000,
                  headers: {
                      Authorization: "Bearer "+ accessTokenObject.get()
                  }
              }).then(function(response){
                  console.log(response.data.recognition);
                  o.selected = response.data.recognition;
                  o.refresh();
                  return 'success';
              }, function(response){
                  o.response = response;
                  checkConnection();
                  return 'fail';
              });
          };
          function getRecipientStr(recipients) {
              var strRecipients = recipients[0].email;

              for(var i=1; i<recipients.length; i++)
              {
                  strRecipients = strRecipients + "," + recipients[i].email;
              }

              console.log(strRecipients);
              return strRecipients;
          }
          o.deleteRecognition = function(recognition) {
              checkConnection();
              return $http({
                  method: "DELETE",
                  url: SERVER.api_url + 'recognitions/' + recognition.id,
                  headers: {
                      Authorization: "Bearer "+ accessTokenObject.get()
                  }
              }).then(function(response){
                  console.log("delete success!!!");
                  o.refresh();
                  return 'success';
              }, function(response){
                  console.log("delete failed!!!");
                  return 'fail';
              });
          };
          o.receiveRecognition = function(recognitionId) {
              checkConnection();

              return $http({
                  method: 'GET',
                  url: SERVER.api_url +  'recognitions/' + recognitionId,
                  headers: {
                      Authorization: "Bearer "+ accessTokenObject.get()
                  }
              }).then(function(response){
                  console.log('read success');
                  o.selected = response.data.recognition;
                  $rootScope.$broadcast('recognitions:received', o.selected);
                  o.refresh();
                  return 'success';
              }, function(response){
                  console.log('read fail');
                  return 'fail';
              });
          };

          o.clear = function() {
              o.recognitions = [];
              read_count = 0;
              has_more = true;
          };

          o.refresh = function() {
              if (navigator.connection && navigator.connection.type === Connection.NONE) {
                  return;
              }

              o.clear();
              $rootScope.$broadcast('recognitions:refresh');
          };

          function checkConnection() {
              var connectionNone = window.Connection ? window.Connection.NONE : "none";

              if (!offlineMode && navigator.connection && navigator.connection.type === connectionNone) {
                  offlineMode = true;
                  if (storedRecognitions) {
                      o.recognitions = JSON.parse(storedRecognitions);
                  } else {
                      $ionicPopup.alert({
                          title: "Houston We Have a Problem",
                          template: "Please check your internet connection."
                      });
                  }
              } else {
                  offlineMode = false;
              }
          }

          return o;
      })

})();
